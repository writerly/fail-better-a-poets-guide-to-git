* The Poem

#+begin_verse
Moore's Chameleon


Too reckless  
to define the 
   agile basilisk 

Herself
wouldn't veil 
   your round 

Anatomy 
in opposition to 
   opal need

Her 
economy of 
   tightened wires 

Pacing
knots where 
   revolution laid 

Upon emerald 
fire laid down 
   her knife 
   
Fwshhh!
She returns down-scale 
   Knees bent

Shoulders 
rolled hands 
   fluttered like 

The low-
chromatic corruption 
   hope stirs 

We delay 
loud the system 
   embedded by 

Tumor 
where a fugitive 
   king hid 

The direction
of power
   Impact 

And exactitude 
polished precision 
   flashes 

Extolled 
liberty mitigated 
   to almost harmless 
#+end_verse
