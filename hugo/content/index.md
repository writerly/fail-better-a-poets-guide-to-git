+++
title = "A Poet's Guide to Git"
author = ["Michael Ryan Peter"]
description = "This document catalogs an annotated list of resources and a workflow example to help creative writers, poets in particular, learn how to incorporate version control with Git into their writing workflow."
lastmod = 2020-06-23T12:13:42-04:00
tags = ["Git", "Poetry"]
categories = ["tech-writing"]
draft = false
weight = 2001
+++

![](/ox-hugo/writerly-logo.png)
![](images/valery-quote.png)


## Introduction {#introduction}

Revision is the crux of writing. As writers, we need tools adapted to a complex workflow built on the process of revision. Tools that allow us to track changes, compare different versions, and document the history of a project over time. Tools that do not interfere with the writing process and that help us to delve into deeper flow states, secure in the knowledge that our work will be preserved.

In this quickstart guide, we are going to write a poem and track revisions using a version control system called Git. Version control systems (VCS) are widely used in software development, where tracking changes and documenting the production process is an integrated part of the workflow. Git is a distributed VCS, meaning it is a tool for managing complexity and the revision process in a collaborative, multi-authored setting.

Git has a reputation as a powerful and flexible tool with a steep learning curve. Luckily, the most complex poem in the world is far simpler, on a mechanical level at least, than even the most basic software. So, writers don't need to use the more advanced Git functions. A handful of fairly straightforward concepts and commands will unlock extensive version control for the dark art of writing poetry.&nbsp;[^fn:1]

{{< figure src="/ox-hugo/moore-quote.png" >}}


## Get Started {#get-started}


### Tutorials and Tools {#tutorials-and-tools}

Before we begin, I suggest you check out a tutorial to familiarize yourself with Git. The YouTube series [Git and GitHub for Poets](https://www.youtube.com/watch?v=BCQHnlnPusY) is a fun introduction to the subject with lots of detail and engaging exercises. I didn't work through all of this series, but simply watching the videos for a week while doing the dishes helped me wrap my head around the subject.

[How writers can get work done better with Git](https://opensource.com/article/19/4/write-git) by Seth Kenlon was the first tutorial on Git that really clicked for me. As a bonus, Kenlon introduces the Atom text editor, writing in plain text, and working with a remote repository in GitLab.

I attribute any success I have in explaining Git to Lorna Mitchell's [Git Workbook](https://leanpub.com/gitworkbook). I came across [Learn Git in a Month of Lunches](https://www.manning.com/books/learn-git-in-a-month-of-lunches) late in writing this guide, but it comes highly recommended, and I love Rick Umali's crisp prose.

[Learn Git Branching](https://learngitbranching.js.org/) is an interactive series of exercises to learn how to move around in Git. This resource does a good job of getting your hands dirty with Git. It starts easy, then quickly escalates in difficulty. Maybe wait to try this one until you are feeling confident in your progress and up for a challenge.[^fn:2]

On the other end of the spectrum, [Git - The Simple Guide](http://rogerdudler.github.io/git-guide/) is among the most concise and useful references I have come across. I recommend keeping it open in your browser as you follow along.

We will also need a dedicated text editor, and not a word processor like MS Word, Google Docs, or Libre Office. Initially, this guide was built around the Atom editor due to its widespread popularity and its built-in graphical user interface (GUI) for Git. If you don't already have a favorite text editor, it does a lot to ease the transition to plain text writing.

Though we will be working in the command line, you will probably find it helpful to download a GUI for Git. This will help you visualize the changes we will make to our repository and ease troubleshooting if issues arise. I like [GitKraken](https://www.gitkraken.com/) for my GUI, but [Git Cola](https://git-cola.github.io/), [gitk](https://git-scm.com/docs/gitk), and [Tower](https://www.git-tower.com/mac) are also popular choices.

If working in the command line is intimidating, [A Non-Nerd's Guide to the Command Line](http://commandline.guide/) is a friendly introduction for the complete novice and quite useful. [The Command Line Crash Course](http://cglab.ca/~morin/teaching/1405/clcc/book/cli-crash-courseli1.html) by Zed Shaw is a deep-dive into the subject that doesn't require any previous experience.

{{< figure src="/ox-hugo/WCW-quote.png" >}}


### My Writing Process {#my-writing-process}

One of the ways I keep my writing life happy and productive is to always have something to revise. The source material could be anything: reading notes, found text, or writing exercises are the most common.

Of all the approaches, procedural writing exercises are my favorite. They allow me to work quickly and intuitively with found text and software. I play with the language until the piece begins to talk like a poem, then set it aside. Sooner or later, I will stumble across the piece, and I will not recognize it as something I wrote. Sometimes, the poem will require little to no editing. In other instances, it will sync up beautifully with another project.

Before Git, it was difficult to keep track of source materials, references, and revisions, as well as explanations on why I made certain decisions during the writing process. Now that I write with my projects under version control, this is no longer an issue.


### Four Commands to Know: `commit`, `diff`, `branch`, and `merge` {#four-commands-to-know-commit-diff-branch-and-merge}

Understanding just four commands allowed me to effectively use Git in my writing workflow. If you can write a good commit message, compare edits and discrepancies between versions of a file, and modularize drastic changes, you will reap the benefits.


#### `commit` {#commit}

The most important habit to develop while learning Git is also the easiest: write good commit messages. After you have made a series of revisions to a document, you need to "stage" the file in Git (`git add`) and then commit those changes to the repository. The repository (or _repo_) is the database that Git uses to track files and their respective changes.

<div class="note">
  <div></div>

It can be tricky to wrap your head around the differences between saving, staging, and committing a file. [Atlassian](https://www.atlassian.com/git/tutorials/saving-changes) has a great explanation. Ignore the parts referring to SVN---this is a competing VCS.

</div>

Many of the articles listed in the References section below will go over what makes a good commit message. I have found the template in [Jacob Herrington's How to Write Useful Commit Messages](https://dev.to/jacobherrington/how-to-write-useful-commit-messages-my-commit-message-template-20n9) to be the most straightforward. His example pertains to writing code, but his template is very useful for writers.

```text
Summarize the change in fewer than 50 characters
(Use a present tense verb where appropriate)

Because:
- Explain the reasons you made this change
- Make a new bullet for each reason

Explain exactly what was done in this commit with more depth than the
50 character subject line.

Include any additional notes, relevant links, or co-authors.
```

This is a good starting point, but over time you may find yourself riffing on Herrington's template. Also, not every commit message will require all of the information outlined above.

Naturally, the creative process is seldom an orderly one, but there are ways to fix that.[^fn:3] For now, let's pretend that we always know what we are doing and why, even if we don't always bundle our revisions into neat, logical commits.

As you get more comfortable with Git and commit messages, start paying attention to how you organize and phrase your `commit` in the first place. Creating logical, explanatory commits that are organized around a task, problem, or new content is difficult. It requires a systematic approach to your workflow. Try your best. Be patient and kind as you strive towards mastery. With time and discipline, the creative process can coalesce towards order.


#### `diff` {#diff}

Between staging a file (`git add`) and committing it to the repo (`git commit`) it is useful to take a closer look at what changes will be included.

The `git diff` command compares changes to our repository. For our purposes, we will just use it at the most basic level to see our most recent changes. This only scratches the surface of what `diff` can do.[^fn:4] Once you have more of a foundation in moving around in Git, tagging commits, and using the `git log`, there is a whole world to explore. For now, playing around in the GUI of your choice will show you some of what `diff` is capable of.


#### `branch` and `merge` {#branch-and-merge}

Branches are central to working collaboratively in Git. Branches allow programmers to work on specific features or to fix problems in isolation from the rest of the project.

It is also a place to try out a crazy new idea that you want to try. Add a subplot, switch tenses, work on whatever strikes your fancy. If you are satisfied with the changes, awesome. If not, you can abandon them and your `master` branch is left unaffected.

When the new edition or edit is complete, you can merge the branch into the main repository.


#### Extra Credit: `git status` and `git log` {#extra-credit-git-status-and-git-log}

The `git status` and `git log` commands will help you keep track of where you are, what has changed, and whether or not your actions have worked out the way you want them to. Lorna Mitchell explains the commands' usefulness  in her talk [Advanced GIT for Developers](https://www.youtube.com/watch?v=duqBHik7nRo). Both commands are covered extensively in her [Git Workbook](https://leanpub.com/gitworkbook).[^fn:5] Lastly, once you are comfortable with `git status` and `git diff`, give `git status -v [filename]` a whirl. It will print a `status` and `diff` report, side by side!

{{< figure src="/ox-hugo/beckett-quote.png" >}}


### Write with Git {#write-with-git}


#### Step Zero: Install, Config, and Init {#step-zero-install-config-and-init}

<div class="important">
  <div></div>

This exercise assumes that you have installed Git. If you have not, [Git - The Simple Guide](http://gitimmersion.com/index.html) will get you started. In the exercises that follow, when you see the $ followed by text, those are commands for you to type in or copy into your terminal prompt. You do not need to include the $.

</div>

If you haven't done so already, enter the following into the terminal to setup your Git configuration (making substitutions where appropriate).

<div class="note">
  <div></div>

In the following example, I took the liberty of setting up Atom as Git's core editor. For a full list of common text editors, visit [Pro Git](https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Setup-and-Config#%5Fcore%5Feditor%20).

Also, Windows users might need to include the full program path to their editor of choice. As always, Pro Git has the answers.

</div>

```text
$ git config --global user.name "Jane Doe"
$ git config --global user.email janedoe@example.com
$ git config --global core.editor "atom --wait"
```

Now, create a folder called `workflow-example` in your Home directory. Create and save an empty file called `poem.txt` in a text editor or in the command line.

```text
$ cd workflow-example/
$ git status
$ git init
$ git status
$ git add poem.txt
$ git status
$ git commit
  - Type "First Commit" for your commit message and save.
$ git status
$ git log
```


#### Step One: Source Material {#step-one-source-material}

The first step in generative or procedural writing is to find rich source material. I began with the article [The Differences Between Warren and Sanders Matter](https://www.theatlantic.com/ideas/archive/2020/02/differences-between-warren-and-sanders-matter/605971/) by Franklin Foer in _The Atlantic_, but any news article will suffice. Find a paragraph that interests you and copy it. I chose the paragraph beginning with, "If Warren wanted to define herself ..." Paste the paragraph into `poem.txt` and save.

Return to the terminal, and make sure you are still in your `workflow-example` directory.

```text
$ git status
$ git add poem.txt
$ git diff poem.txt
$ git commit
```

Alright, let's write an excellent commit message based on our template.

```text
Source: Differences Between Warren and Sanders Matter

Because:
- Idea Words
- Current Event
- Tab

Reference:
https://www.theatlantic.com/ideas/archive/2020/02/differences-between-warren-and-sanders-matter/605971/

Author:
Franklin Foer
```


#### Step Two: Make a Revision {#step-two-make-a-revision}

Point your browser to <https://appliedpoetics.org>. This site hosts tools to mangle, distort, and otherwise mine source texts. I used the Lipogram function under the Oulipean menu with the constraint of the letter _A_. Play around for a few minutes, but try to find something that resonates with you without thinking about it too much. Copy and save the resulting text into `poem.txt`.

Open the terminal.

```text
$ git status
$ git add poem.txt
$ git status
$ git diff --color-words poem.txt
$ git commit
  - Write a good commit message based on the template.
    - Reference https://appliedpoetics.org.
    - Make note of the constraint you used and why.
    - What do you like about the text?
    - What would you like to change?
    - What other directions could you see pursuing?
  - Save your commit message and close.
$ git log
```


#### Step Three: Enjamb {#step-three-enjamb}

Make some line breaks. Move text around. Look at the original article and see if the algorithm stripped away words you want to keep. For instance, I was sad that _mitigated_ didn't make it through. So, I put it back in. Save your file.

```text
$ git status
$ git add poem.txt
$ git status
$ git diff --color-words poem.txt
$ git commit
  - Write a useful, clear, and concise commit message
$ git log
```


#### Step Four: Add More Voices {#step-four-add-more-voices}

Okay, this is starting to look like a poem. Mine is pretty one-dimensional, but I like the tone. A big problem is that it is very abstract. A good poem needs imagery, texture, and musicality. Let's try to add some more interesting language to the mix, shall we? Literary criticism is a great resource for cut up materials because criticism contains a diverse array of primary texts and theory.

<div class="reminder">
  <div></div>

If you are coming back to your project after a bit of a break, remember you can pull up `git log` to take a peek at your comments. Fortunately, we write good commit messages!

</div>

I chose a review of Marianne Moore's _New Collected Poems_, ["Willing to be Reckless"](https://www.poetryfoundation.org/poetrymagazine/articles/144804/willing-to-be-reckless) by Ange Mlinko. Moore is famous for revising her poems for decades after their initial publication,so she's a poet who would have loved a VCS. Mlinko quotes and discusses Moore's "The Chameleon" at length, which will lend musicality and strong imagery to our work-in-progress poem.

Use "Willing to be Reckless" or find your own text. Whichever you choose, pick something longer than just a paragraph. Head over to <https://appliedpoetics.org>. Find a function you like. I chose "Coloring Book" under the "Pop Culture" menu.

Make a new file, such as `reckless.txt`, copy the generated text, and save. Run `git status` in the terminal.

Now, let's create a new branch to compartmentalize the changes we are about to make.


#### Step Five: See the Branches for the Trunk {#step-five-see-the-branches-for-the-trunk}

So far, we have been working on a single branch on our Git repository, the default or "master" branch. Git makes it easy to experiment with new features, story arcs, rhyme schemes, or bug fixes while keeping the original document intact. Once we are satisfied with our experimental branch, it is simple to merge our changes into the master branch. Though it is beyond the scope of this tutorial, it is even possible to pick and choose specific commits from different branches and merge them into the master branch. Pretty powerful stuff! This also allows for multiple editors or authors to work in parallel with each other through an external repository.

<div class="caution">
  <div></div>

There is no rule against working on the master branch while working on the feature branch. In fact, Git was designed to facilitate multiple contributors working independently and simultaneously on the same files.

Sometimes, when we merge branches, a merge conflict will occur. This sounds scary, but it is pretty easy to fix, especially with a GUI. [Git Tower](https://www.git-tower.com/learn/git/ebook/en/command-line/advanced-topics/merge-conflicts) has an excellent article on how to deal with merge conflicts. We are currently working with a simplified workflow to avoid these merge conflicts.&nbsp;[^fn:6]

</div>

Okay, fire up the terminal.

```text
$ git checkout -b reckless
  - This creates a new branch named reckless and switches to it.
$ git add reckless.txt
$ git status
$ git commit
  - Be sure to note your reference material and generative tactic used.
  - How do you see the "base" poem in light of your newly generated material?
  - How will you shape this new text?
  - How will you revise the old?
$ git log
```

Congratulations, you have now made a new branch! Now let's start playing around with our new source material. As before, edit `reckless.txt` and play around with line breaks until the computer generated text has the look and feel of a poem. Try to work quickly and intuitively. Once you are satisfied, save your file.

Let's add this file to our repository.

```text
$ git status
$ git add reckless.txt
$ git diff --color-words reckless.txt
$ git commit
$ git log
```

<div class="caution">
  <div></div>

If you create a file on `branch b`, when you switch to `branch a` the new file will not be there unless you merge the branch or pull the file. It can be confusing at first, but this is a feature and not a bug.

If it ever seems like a file has disappeared, be sure to check your branches. This is another case where having a GUI will help.[^fn:7]

When you are comfortable, try an experimental repo with weird branching. Like a calendar house, or memory palace, or a journal with a new branch for each day of the week.

</div>

<div class="tip">
  <div></div>

The common advice is to "branch early and branch often."

</div>


#### Step Six: Merge with Master {#step-six-merge-with-master}

In your text editor, combine reckless.txt into poem.txt. Save the file, make sure you are on the `reckless` branch, stage it, compare changes, and commit. (You've got this!)

<div class="note">
  <div></div>

When you find yourself 75% happy with the poem, stop. Immediately. Stop right at the point where changes begin to make things better _and_ worse. Stop when you can glimpse the perfect poem just around the corner, but it is starting to slip through your fingers.

Write a detailed commit message describing what you want to change, can't figure out, what annoys you, and what you love.

Now shut down your computer, go for a walk, and do everything you can to forget the poem. One day, in the near or distant future, the poem will resurface. It will have blossomed into a mysterious signal from a distant star. Thanks to our wonderfully written commit messages, we have the best of encountering this bizarre artifact unadorned along with a full background of our intent, allusions, and references.

</div>

Before we go, let's `merge` into our `master` branch, shall we? That way we don't lose our file on an orphan branch. Open the terminal and begin.

```text
$ git checkout master
$ git merge reckless

Pump your fist in the air!
```

You wrote a poem and learned some Git! Your life is forever changed.


## About Me {#about-me}

I am a technical writer, poet, and a wine and spirits industry professional. If I am not dancing to Sun Ra with my toddler son, I might be cooking dinner or messing with my Emacs configuration. Sometimes, I make time for writing and Taoist mediation.

Thanks,
_Michael_


## The Poem {#the-poem}

<p class="verse">
Moore's Chameleon<br />
<br />
<br />
Too reckless<br />
to define the<br />
&nbsp;&nbsp;&nbsp;agile basilisk<br />
<br />
Herself<br />
wouldn't veil<br />
&nbsp;&nbsp;&nbsp;your round<br />
<br />
Anatomy<br />
in opposition to<br />
&nbsp;&nbsp;&nbsp;opal need<br />
<br />
Her<br />
economy of<br />
&nbsp;&nbsp;&nbsp;tightened wires<br />
<br />
Pacing<br />
knots where<br />
&nbsp;&nbsp;&nbsp;revolution laid<br />
<br />
Upon emerald<br />
fire laid down<br />
&nbsp;&nbsp;&nbsp;her knife<br />
<br />
Fwshhh!<br />
She returns down-scale<br />
&nbsp;&nbsp;&nbsp;Knees bent<br />
<br />
Shoulders<br />
rolled hands<br />
&nbsp;&nbsp;&nbsp;fluttered like<br />
<br />
The low-<br />
chromatic corruption<br />
&nbsp;&nbsp;&nbsp;hope stirs<br />
<br />
We delay<br />
loud the system<br />
&nbsp;&nbsp;&nbsp;embedded by<br />
<br />
Tumor<br />
where a fugitive<br />
&nbsp;&nbsp;&nbsp;king hid<br />
<br />
The direction<br />
of power<br />
&nbsp;&nbsp;&nbsp;Impact<br />
<br />
And exactitude<br />
polished precision<br />
&nbsp;&nbsp;&nbsp;flashes<br />
<br />
Extolled<br />
liberty mitigated<br />
&nbsp;&nbsp;&nbsp;to almost harmless<br />
</p>

[^fn:1]: There is absolutely no shame in deciding that Git is not for you, and I don't say this because learning Git is difficult. The technical challenges are not trivial, but neither are they insurmountable. In the end, all that matters is that you are happy with what you write, and that you are happy while you are writing. If you have a system that works for you, then please take everything that follows with a grain of salt. While there are benefits to learning a VCS, no one should implement a tool for the sake of the tool. This is as true for Git as it is for Word. The last thing I want to encourage is a kind of elitist, hipster tool fetish. _He typed in an obscure text editor, copied from a draft he wrote with a fountain pen on Japanese stationery._
[^fn:2]: [Git Katas](https://github.com/praqma-training/git-katas) are exercises for daily, deliberate practice. [How To Use Git to Manage Your Writing Project](https://www.digitalocean.com/community/tutorials/how-to-use-git-to-manage-your-writing-project) is geared specifically towards writers, with details on how to export old versions or save a comparison of two versions of file. Lastly, [Pro Git](https://git-scm.com/book/en/v2), [Atlassian](https://www.atlassian.com/git), and [Git Immersion](http://gitimmersion.com/index.html) all take different approaches to learning Git, and each has a lot to offer.
[^fn:3]: The command `git add -p` allows you to interactively group a series of interrelated revisions into one commit, even across multiple files. Think of changing the name of a lead character across many chapters. `git rebase -i` allows you to pick through a series of commits and, essentially, rewrite history into more logically structured commits with clearer commit messages. These are pretty advanced topics, so I don't recommend jumping right in with these commands. Lorna Mitchell's Git Workbook does a great job explaining both concepts. [Deliberate Git](https://www.rakeroutes.com/deliberate-git/) also goes into using `rebase -i` to rewrite commits and commit messages to create a human readable _history_ of a project, as opposed to a _log_.
[^fn:4]: <https://git-scm.com/docs/git-diff>
[^fn:5]: Have I plugged her book enough yet?
[^fn:6]: Once again, I love Lorna Mitchell's [Git Workbook](https://leanpub.com/gitworkbook) because she sets up errors and then walks you through fixing them.
[^fn:7]: While you are working, remember that `git status` lets you know what branch you are on, what changes Git is or isn't tracking, and what you have added to the staging area. After you commit, `git log` lets you look back to make sure everything happened the way that you expected, and lets you take a look at your commit messages.
