<!DOCTYPE html>
<html lang="en" dir=>

<head>
  <meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="This document catalogs an annotated list of resources and a workflow example to help creative writers, poets in particular, learn how to incorporate version control with Git into their writing workflow.">
<meta name="theme-color" content="#FFFFFF"><meta property="og:title" content="A Poet&#39;s Guide to Git" />
<meta property="og:description" content="This document catalogs an annotated list of resources and a workflow example to help creative writers, poets in particular, learn how to incorporate version control with Git into their writing workflow." />
<meta property="og:type" content="article" />
<meta property="og:url" content="http://writerly.dev/" />
<meta property="article:published_time" content="2020-06-23T12:13:42-04:00" />
<meta property="article:modified_time" content="2020-06-23T12:13:42-04:00" />
<title>A Poet&#39;s Guide to Git | writerly.dev</title>
<link rel="manifest" href="/manifest.json">
<link rel="icon" href="/favicon.png" type="image/x-icon">
<link rel="stylesheet" href="/book.min.e40449dffab36960ca7d6bac529d52d7ab9f9d230ffe5de1799e29cb61bc0cb7.css" integrity="sha256-5ARJ3/qzaWDKfWusUp1S16ufnSMP/l3heZ4py2G8DLc=">
<script defer src="/en.search.min.ab29c2eae298146616d343c2e8fddb2226eaa8c6dd22cdb5cab2bb5c24c5a1b8.js" integrity="sha256-qynC6uKYFGYW00PC6P3bIibqqMbdIs21yrK7XCTFobg="></script>
<!--
Made with Book Theme
https://github.com/alex-shpak/hugo-book
-->

  
</head>

<body dir=>
  <input type="checkbox" class="hidden" id="menu-control" />
  <main class="container flex">
    <aside class="book-menu">
      
  <nav>
<h2 class="book-brand">
  <a href="/"><span>writerly.dev</span>
  </a>
</h2>


<div class="book-search">
  <input type="text" id="book-search-input" placeholder="Search" aria-label="Search" maxlength="64" data-hotkeys="s/" />
  <div class="book-search-spinner hidden"></div>
  <ul id="book-search-results"></ul>
</div>











  
















</nav>




  <script>(function(){var menu=document.querySelector("aside.book-menu nav");addEventListener("beforeunload",function(event){localStorage.setItem("menu.scrollTop",menu.scrollTop);});menu.scrollTop=localStorage.getItem("menu.scrollTop");})();</script>


 
    </aside>

    <div class="book-page">
      <header class="book-header">
        
  <div class="flex align-center justify-between">
  <label for="menu-control">
    <img src="/svg/menu.svg" class="book-icon" alt="Menu" />
  </label>

  <strong>A Poet&#39;s Guide to Git</strong>

  <label for="toc-control">
    
    <img src="/svg/toc.svg" class="book-icon" alt="Table of Contents" />
    
  </label>
</div>


  
    <input type="checkbox" class="hidden" id="toc-control" />
    <aside class="hidden clearfix">
      
  <nav id="TableOfContents">
  <ul>
    <li><a href="#introduction">Introduction</a></li>
    <li><a href="#get-started">Get Started</a>
      <ul>
        <li><a href="#tutorials-and-tools">Tutorials and Tools</a></li>
        <li><a href="#my-writing-process">My Writing Process</a></li>
        <li><a href="#four-commands-to-know-commit-diff-branch-and-merge">Four Commands to Know: <code>commit</code>, <code>diff</code>, <code>branch</code>, and <code>merge</code></a></li>
        <li><a href="#write-with-git">Write with Git</a></li>
      </ul>
    </li>
    <li><a href="#about-me">About Me</a></li>
    <li><a href="#the-poem">The Poem</a></li>
  </ul>
</nav>


    </aside>
  
 
      </header>

      
      
  <article class="markdown"><p><img src="/ox-hugo/writerly-logo.png" alt="" />
<img src="images/valery-quote.png" alt="" /></p>
<h2 id="introduction">
  Introduction
  <a class="anchor" href="#introduction">#</a>
</h2>
<p>Revision is the crux of writing. As writers, we need tools adapted to a complex workflow built on the process of revision. Tools that allow us to track changes, compare different versions, and document the history of a project over time. Tools that do not interfere with the writing process and that help us to delve into deeper flow states, secure in the knowledge that our work will be preserved.</p>
<p>In this quickstart guide, we are going to write a poem and track revisions using a version control system called Git. Version control systems (VCS) are widely used in software development, where tracking changes and documenting the production process is an integrated part of the workflow. Git is a distributed VCS, meaning it is a tool for managing complexity and the revision process in a collaborative, multi-authored setting.</p>
<p>Git has a reputation as a powerful and flexible tool with a steep learning curve. Luckily, the most complex poem in the world is far simpler, on a mechanical level at least, than even the most basic software. So, writers don&rsquo;t need to use the more advanced Git functions. A handful of fairly straightforward concepts and commands will unlock extensive version control for the dark art of writing poetry. <sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup></p>
<figure>
    <img src="/ox-hugo/moore-quote.png"/> 
</figure>

<h2 id="get-started">
  Get Started
  <a class="anchor" href="#get-started">#</a>
</h2>
<h3 id="tutorials-and-tools">
  Tutorials and Tools
  <a class="anchor" href="#tutorials-and-tools">#</a>
</h3>
<p>Before we begin, I suggest you check out a tutorial to familiarize yourself with Git. The YouTube series <a href="https://www.youtube.com/watch?v=BCQHnlnPusY">Git and GitHub for Poets</a> is a fun introduction to the subject with lots of detail and engaging exercises. I didn&rsquo;t work through all of this series, but simply watching the videos for a week while doing the dishes helped me wrap my head around the subject.</p>
<p><a href="https://opensource.com/article/19/4/write-git">How writers can get work done better with Git</a> by Seth Kenlon was the first tutorial on Git that really clicked for me. As a bonus, Kenlon introduces the Atom text editor, writing in plain text, and working with a remote repository in GitLab.</p>
<p>I attribute any success I have in explaining Git to Lorna Mitchell&rsquo;s <a href="https://leanpub.com/gitworkbook">Git Workbook</a>. I came across <a href="https://www.manning.com/books/learn-git-in-a-month-of-lunches">Learn Git in a Month of Lunches</a> late in writing this guide, but it comes highly recommended, and I love Rick Umali&rsquo;s crisp prose.</p>
<p><a href="https://learngitbranching.js.org/">Learn Git Branching</a> is an interactive series of exercises to learn how to move around in Git. This resource does a good job of getting your hands dirty with Git. It starts easy, then quickly escalates in difficulty. Maybe wait to try this one until you are feeling confident in your progress and up for a challenge.<sup id="fnref:2"><a href="#fn:2" class="footnote-ref" role="doc-noteref">2</a></sup></p>
<p>On the other end of the spectrum, <a href="http://rogerdudler.github.io/git-guide/">Git - The Simple Guide</a> is among the most concise and useful references I have come across. I recommend keeping it open in your browser as you follow along.</p>
<p>We will also need a dedicated text editor, and not a word processor like MS Word, Google Docs, or Libre Office. Initially, this guide was built around the Atom editor due to its widespread popularity and its built-in graphical user interface (GUI) for Git. If you don&rsquo;t already have a favorite text editor, it does a lot to ease the transition to plain text writing.</p>
<p>Though we will be working in the command line, you will probably find it helpful to download a GUI for Git. This will help you visualize the changes we will make to our repository and ease troubleshooting if issues arise. I like <a href="https://www.gitkraken.com/">GitKraken</a> for my GUI, but <a href="https://git-cola.github.io/">Git Cola</a>, <a href="https://git-scm.com/docs/gitk">gitk</a>, and <a href="https://www.git-tower.com/mac">Tower</a> are also popular choices.</p>
<p>If working in the command line is intimidating, <a href="http://commandline.guide/">A Non-Nerd&rsquo;s Guide to the Command Line</a> is a friendly introduction for the complete novice and quite useful. <a href="http://cglab.ca/~morin/teaching/1405/clcc/book/cli-crash-courseli1.html">The Command Line Crash Course</a> by Zed Shaw is a deep-dive into the subject that doesn&rsquo;t require any previous experience.</p>
<figure>
    <img src="/ox-hugo/WCW-quote.png"/> 
</figure>

<h3 id="my-writing-process">
  My Writing Process
  <a class="anchor" href="#my-writing-process">#</a>
</h3>
<p>One of the ways I keep my writing life happy and productive is to always have something to revise. The source material could be anything: reading notes, found text, or writing exercises are the most common.</p>
<p>Of all the approaches, procedural writing exercises are my favorite. They allow me to work quickly and intuitively with found text and software. I play with the language until the piece begins to talk like a poem, then set it aside. Sooner or later, I will stumble across the piece, and I will not recognize it as something I wrote. Sometimes, the poem will require little to no editing. In other instances, it will sync up beautifully with another project.</p>
<p>Before Git, it was difficult to keep track of source materials, references, and revisions, as well as explanations on why I made certain decisions during the writing process. Now that I write with my projects under version control, this is no longer an issue.</p>
<h3 id="four-commands-to-know-commit-diff-branch-and-merge">
  Four Commands to Know: <code>commit</code>, <code>diff</code>, <code>branch</code>, and <code>merge</code>
  <a class="anchor" href="#four-commands-to-know-commit-diff-branch-and-merge">#</a>
</h3>
<p>Understanding just four commands allowed me to effectively use Git in my writing workflow. If you can write a good commit message, compare edits and discrepancies between versions of a file, and modularize drastic changes, you will reap the benefits.</p>
<h4 id="commit">
  <code>commit</code>
  <a class="anchor" href="#commit">#</a>
</h4>
<p>The most important habit to develop while learning Git is also the easiest: write good commit messages. After you have made a series of revisions to a document, you need to &ldquo;stage&rdquo; the file in Git (<code>git add</code>) and then commit those changes to the repository. The repository (or <em>repo</em>) is the database that Git uses to track files and their respective changes.</p>
<!-- raw HTML omitted -->
<p>It can be tricky to wrap your head around the differences between saving, staging, and committing a file. <a href="https://www.atlassian.com/git/tutorials/saving-changes">Atlassian</a> has a great explanation. Ignore the parts referring to SVN&mdash;this is a competing VCS.</p>
<!-- raw HTML omitted -->
<p>Many of the articles listed in the References section below will go over what makes a good commit message. I have found the template in <a href="https://dev.to/jacobherrington/how-to-write-useful-commit-messages-my-commit-message-template-20n9">Jacob Herrington&rsquo;s How to Write Useful Commit Messages</a> to be the most straightforward. His example pertains to writing code, but his template is very useful for writers.</p>
<div class="highlight"><pre style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-text" data-lang="text">Summarize the change in fewer than 50 characters
(Use a present tense verb where appropriate)

Because:
- Explain the reasons you made this change
- Make a new bullet for each reason

Explain exactly what was done in this commit with more depth than the
50 character subject line.

Include any additional notes, relevant links, or co-authors.
</code></pre></div><p>This is a good starting point, but over time you may find yourself riffing on Herrington&rsquo;s template. Also, not every commit message will require all of the information outlined above.</p>
<p>Naturally, the creative process is seldom an orderly one, but there are ways to fix that.<sup id="fnref:3"><a href="#fn:3" class="footnote-ref" role="doc-noteref">3</a></sup> For now, let&rsquo;s pretend that we always know what we are doing and why, even if we don&rsquo;t always bundle our revisions into neat, logical commits.</p>
<p>As you get more comfortable with Git and commit messages, start paying attention to how you organize and phrase your <code>commit</code> in the first place. Creating logical, explanatory commits that are organized around a task, problem, or new content is difficult. It requires a systematic approach to your workflow. Try your best. Be patient and kind as you strive towards mastery. With time and discipline, the creative process can coalesce towards order.</p>
<h4 id="diff">
  <code>diff</code>
  <a class="anchor" href="#diff">#</a>
</h4>
<p>Between staging a file (<code>git add</code>) and committing it to the repo (<code>git commit</code>) it is useful to take a closer look at what changes will be included.</p>
<p>The <code>git diff</code> command compares changes to our repository. For our purposes, we will just use it at the most basic level to see our most recent changes. This only scratches the surface of what <code>diff</code> can do.<sup id="fnref:4"><a href="#fn:4" class="footnote-ref" role="doc-noteref">4</a></sup> Once you have more of a foundation in moving around in Git, tagging commits, and using the <code>git log</code>, there is a whole world to explore. For now, playing around in the GUI of your choice will show you some of what <code>diff</code> is capable of.</p>
<h4 id="branch-and-merge">
  <code>branch</code> and <code>merge</code>
  <a class="anchor" href="#branch-and-merge">#</a>
</h4>
<p>Branches are central to working collaboratively in Git. Branches allow programmers to work on specific features or to fix problems in isolation from the rest of the project.</p>
<p>It is also a place to try out a crazy new idea that you want to try. Add a subplot, switch tenses, work on whatever strikes your fancy. If you are satisfied with the changes, awesome. If not, you can abandon them and your <code>master</code> branch is left unaffected.</p>
<p>When the new edition or edit is complete, you can merge the branch into the main repository.</p>
<h4 id="extra-credit-git-status-and-git-log">
  Extra Credit: <code>git status</code> and <code>git log</code>
  <a class="anchor" href="#extra-credit-git-status-and-git-log">#</a>
</h4>
<p>The <code>git status</code> and <code>git log</code> commands will help you keep track of where you are, what has changed, and whether or not your actions have worked out the way you want them to. Lorna Mitchell explains the commands&rsquo; usefulness  in her talk <a href="https://www.youtube.com/watch?v=duqBHik7nRo">Advanced GIT for Developers</a>. Both commands are covered extensively in her <a href="https://leanpub.com/gitworkbook">Git Workbook</a>.<sup id="fnref:5"><a href="#fn:5" class="footnote-ref" role="doc-noteref">5</a></sup> Lastly, once you are comfortable with <code>git status</code> and <code>git diff</code>, give <code>git status -v [filename]</code> a whirl. It will print a <code>status</code> and <code>diff</code> report, side by side!</p>
<figure>
    <img src="/ox-hugo/beckett-quote.png"/> 
</figure>

<h3 id="write-with-git">
  Write with Git
  <a class="anchor" href="#write-with-git">#</a>
</h3>
<h4 id="step-zero-install-config-and-init">
  Step Zero: Install, Config, and Init
  <a class="anchor" href="#step-zero-install-config-and-init">#</a>
</h4>
<!-- raw HTML omitted -->
<p>This exercise assumes that you have installed Git. If you have not, <a href="http://gitimmersion.com/index.html">Git - The Simple Guide</a> will get you started. In the exercises that follow, when you see the $ followed by text, those are commands for you to type in or copy into your terminal prompt. You do not need to include the $.</p>
<!-- raw HTML omitted -->
<p>If you haven&rsquo;t done so already, enter the following into the terminal to setup your Git configuration (making substitutions where appropriate).</p>
<!-- raw HTML omitted -->
<p>In the following example, I took the liberty of setting up Atom as Git&rsquo;s core editor. For a full list of common text editors, visit <a href="https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Setup-and-Config#%5Fcore%5Feditor%20">Pro Git</a>.</p>
<p>Also, Windows users might need to include the full program path to their editor of choice. As always, Pro Git has the answers.</p>
<!-- raw HTML omitted -->
<div class="highlight"><pre style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-text" data-lang="text">$ git config --global user.name &#34;Jane Doe&#34;
$ git config --global user.email janedoe@example.com
$ git config --global core.editor &#34;atom --wait&#34;
</code></pre></div><p>Now, create a folder called <code>workflow-example</code> in your Home directory. Create and save an empty file called <code>poem.txt</code> in a text editor or in the command line.</p>
<div class="highlight"><pre style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-text" data-lang="text">$ cd workflow-example/
$ git status
$ git init
$ git status
$ git add poem.txt
$ git status
$ git commit
  - Type &#34;First Commit&#34; for your commit message and save.
$ git status
$ git log
</code></pre></div><h4 id="step-one-source-material">
  Step One: Source Material
  <a class="anchor" href="#step-one-source-material">#</a>
</h4>
<p>The first step in generative or procedural writing is to find rich source material. I began with the article <a href="https://www.theatlantic.com/ideas/archive/2020/02/differences-between-warren-and-sanders-matter/605971/">The Differences Between Warren and Sanders Matter</a> by Franklin Foer in <em>The Atlantic</em>, but any news article will suffice. Find a paragraph that interests you and copy it. I chose the paragraph beginning with, &ldquo;If Warren wanted to define herself &hellip;&rdquo; Paste the paragraph into <code>poem.txt</code> and save.</p>
<p>Return to the terminal, and make sure you are still in your <code>workflow-example</code> directory.</p>
<div class="highlight"><pre style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-text" data-lang="text">$ git status
$ git add poem.txt
$ git diff poem.txt
$ git commit
</code></pre></div><p>Alright, let&rsquo;s write an excellent commit message based on our template.</p>
<div class="highlight"><pre style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-text" data-lang="text">Source: Differences Between Warren and Sanders Matter

Because:
- Idea Words
- Current Event
- Tab

Reference:
https://www.theatlantic.com/ideas/archive/2020/02/differences-between-warren-and-sanders-matter/605971/

Author:
Franklin Foer
</code></pre></div><h4 id="step-two-make-a-revision">
  Step Two: Make a Revision
  <a class="anchor" href="#step-two-make-a-revision">#</a>
</h4>
<p>Point your browser to <a href="https://appliedpoetics.org">https://appliedpoetics.org</a>. This site hosts tools to mangle, distort, and otherwise mine source texts. I used the Lipogram function under the Oulipean menu with the constraint of the letter <em>A</em>. Play around for a few minutes, but try to find something that resonates with you without thinking about it too much. Copy and save the resulting text into <code>poem.txt</code>.</p>
<p>Open the terminal.</p>
<div class="highlight"><pre style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-text" data-lang="text">$ git status
$ git add poem.txt
$ git status
$ git diff --color-words poem.txt
$ git commit
  - Write a good commit message based on the template.
    - Reference https://appliedpoetics.org.
    - Make note of the constraint you used and why.
    - What do you like about the text?
    - What would you like to change?
    - What other directions could you see pursuing?
  - Save your commit message and close.
$ git log
</code></pre></div><h4 id="step-three-enjamb">
  Step Three: Enjamb
  <a class="anchor" href="#step-three-enjamb">#</a>
</h4>
<p>Make some line breaks. Move text around. Look at the original article and see if the algorithm stripped away words you want to keep. For instance, I was sad that <em>mitigated</em> didn&rsquo;t make it through. So, I put it back in. Save your file.</p>
<div class="highlight"><pre style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-text" data-lang="text">$ git status
$ git add poem.txt
$ git status
$ git diff --color-words poem.txt
$ git commit
  - Write a useful, clear, and concise commit message
$ git log
</code></pre></div><h4 id="step-four-add-more-voices">
  Step Four: Add More Voices
  <a class="anchor" href="#step-four-add-more-voices">#</a>
</h4>
<p>Okay, this is starting to look like a poem. Mine is pretty one-dimensional, but I like the tone. A big problem is that it is very abstract. A good poem needs imagery, texture, and musicality. Let&rsquo;s try to add some more interesting language to the mix, shall we? Literary criticism is a great resource for cut up materials because criticism contains a diverse array of primary texts and theory.</p>
<!-- raw HTML omitted -->
<p>If you are coming back to your project after a bit of a break, remember you can pull up <code>git log</code> to take a peek at your comments. Fortunately, we write good commit messages!</p>
<!-- raw HTML omitted -->
<p>I chose a review of Marianne Moore&rsquo;s <em>New Collected Poems</em>, <a href="https://www.poetryfoundation.org/poetrymagazine/articles/144804/willing-to-be-reckless">&ldquo;Willing to be Reckless&rdquo;</a> by Ange Mlinko. Moore is famous for revising her poems for decades after their initial publication,so she&rsquo;s a poet who would have loved a VCS. Mlinko quotes and discusses Moore&rsquo;s &ldquo;The Chameleon&rdquo; at length, which will lend musicality and strong imagery to our work-in-progress poem.</p>
<p>Use &ldquo;Willing to be Reckless&rdquo; or find your own text. Whichever you choose, pick something longer than just a paragraph. Head over to <a href="https://appliedpoetics.org">https://appliedpoetics.org</a>. Find a function you like. I chose &ldquo;Coloring Book&rdquo; under the &ldquo;Pop Culture&rdquo; menu.</p>
<p>Make a new file, such as <code>reckless.txt</code>, copy the generated text, and save. Run <code>git status</code> in the terminal.</p>
<p>Now, let&rsquo;s create a new branch to compartmentalize the changes we are about to make.</p>
<h4 id="step-five-see-the-branches-for-the-trunk">
  Step Five: See the Branches for the Trunk
  <a class="anchor" href="#step-five-see-the-branches-for-the-trunk">#</a>
</h4>
<p>So far, we have been working on a single branch on our Git repository, the default or &ldquo;master&rdquo; branch. Git makes it easy to experiment with new features, story arcs, rhyme schemes, or bug fixes while keeping the original document intact. Once we are satisfied with our experimental branch, it is simple to merge our changes into the master branch. Though it is beyond the scope of this tutorial, it is even possible to pick and choose specific commits from different branches and merge them into the master branch. Pretty powerful stuff! This also allows for multiple editors or authors to work in parallel with each other through an external repository.</p>
<!-- raw HTML omitted -->
<p>There is no rule against working on the master branch while working on the feature branch. In fact, Git was designed to facilitate multiple contributors working independently and simultaneously on the same files.</p>
<p>Sometimes, when we merge branches, a merge conflict will occur. This sounds scary, but it is pretty easy to fix, especially with a GUI. <a href="https://www.git-tower.com/learn/git/ebook/en/command-line/advanced-topics/merge-conflicts">Git Tower</a> has an excellent article on how to deal with merge conflicts. We are currently working with a simplified workflow to avoid these merge conflicts. <sup id="fnref:6"><a href="#fn:6" class="footnote-ref" role="doc-noteref">6</a></sup></p>
<!-- raw HTML omitted -->
<p>Okay, fire up the terminal.</p>
<div class="highlight"><pre style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-text" data-lang="text">$ git checkout -b reckless
  - This creates a new branch named reckless and switches to it.
$ git add reckless.txt
$ git status
$ git commit
  - Be sure to note your reference material and generative tactic used.
  - How do you see the &#34;base&#34; poem in light of your newly generated material?
  - How will you shape this new text?
  - How will you revise the old?
$ git log
</code></pre></div><p>Congratulations, you have now made a new branch! Now let&rsquo;s start playing around with our new source material. As before, edit <code>reckless.txt</code> and play around with line breaks until the computer generated text has the look and feel of a poem. Try to work quickly and intuitively. Once you are satisfied, save your file.</p>
<p>Let&rsquo;s add this file to our repository.</p>
<div class="highlight"><pre style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-text" data-lang="text">$ git status
$ git add reckless.txt
$ git diff --color-words reckless.txt
$ git commit
$ git log
</code></pre></div><!-- raw HTML omitted -->
<p>If you create a file on <code>branch b</code>, when you switch to <code>branch a</code> the new file will not be there unless you merge the branch or pull the file. It can be confusing at first, but this is a feature and not a bug.</p>
<p>If it ever seems like a file has disappeared, be sure to check your branches. This is another case where having a GUI will help.<sup id="fnref:7"><a href="#fn:7" class="footnote-ref" role="doc-noteref">7</a></sup></p>
<p>When you are comfortable, try an experimental repo with weird branching. Like a calendar house, or memory palace, or a journal with a new branch for each day of the week.</p>
<!-- raw HTML omitted -->
<!-- raw HTML omitted -->
<p>The common advice is to &ldquo;branch early and branch often.&rdquo;</p>
<!-- raw HTML omitted -->
<h4 id="step-six-merge-with-master">
  Step Six: Merge with Master
  <a class="anchor" href="#step-six-merge-with-master">#</a>
</h4>
<p>In your text editor, combine reckless.txt into poem.txt. Save the file, make sure you are on the <code>reckless</code> branch, stage it, compare changes, and commit. (You&rsquo;ve got this!)</p>
<!-- raw HTML omitted -->
<p>When you find yourself 75% happy with the poem, stop. Immediately. Stop right at the point where changes begin to make things better <em>and</em> worse. Stop when you can glimpse the perfect poem just around the corner, but it is starting to slip through your fingers.</p>
<p>Write a detailed commit message describing what you want to change, can&rsquo;t figure out, what annoys you, and what you love.</p>
<p>Now shut down your computer, go for a walk, and do everything you can to forget the poem. One day, in the near or distant future, the poem will resurface. It will have blossomed into a mysterious signal from a distant star. Thanks to our wonderfully written commit messages, we have the best of encountering this bizarre artifact unadorned along with a full background of our intent, allusions, and references.</p>
<!-- raw HTML omitted -->
<p>Before we go, let&rsquo;s <code>merge</code> into our <code>master</code> branch, shall we? That way we don&rsquo;t lose our file on an orphan branch. Open the terminal and begin.</p>
<div class="highlight"><pre style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-text" data-lang="text">$ git checkout master
$ git merge reckless

Pump your fist in the air!
</code></pre></div><p>You wrote a poem and learned some Git! Your life is forever changed.</p>
<h2 id="about-me">
  About Me
  <a class="anchor" href="#about-me">#</a>
</h2>
<p>I am a technical writer, poet, and a wine and spirits industry professional. If I am not dancing to Sun Ra with my toddler son, I might be cooking dinner or messing with my Emacs configuration. Sometimes, I make time for writing and Taoist mediation.</p>
<p>Thanks,
<em>Michael</em></p>
<h2 id="the-poem">
  The Poem
  <a class="anchor" href="#the-poem">#</a>
</h2>
<!-- raw HTML omitted -->
<section class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1" role="doc-endnote">
<p>There is absolutely no shame in deciding that Git is not for you, and I don&rsquo;t say this because learning Git is difficult. The technical challenges are not trivial, but neither are they insurmountable. In the end, all that matters is that you are happy with what you write, and that you are happy while you are writing. If you have a system that works for you, then please take everything that follows with a grain of salt. While there are benefits to learning a VCS, no one should implement a tool for the sake of the tool. This is as true for Git as it is for Word. The last thing I want to encourage is a kind of elitist, hipster tool fetish. <em>He typed in an obscure text editor, copied from a draft he wrote with a fountain pen on Japanese stationery.</em> <a href="#fnref:1" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:2" role="doc-endnote">
<p><a href="https://github.com/praqma-training/git-katas">Git Katas</a> are exercises for daily, deliberate practice. <a href="https://www.digitalocean.com/community/tutorials/how-to-use-git-to-manage-your-writing-project">How To Use Git to Manage Your Writing Project</a> is geared specifically towards writers, with details on how to export old versions or save a comparison of two versions of file. Lastly, <a href="https://git-scm.com/book/en/v2">Pro Git</a>, <a href="https://www.atlassian.com/git">Atlassian</a>, and <a href="http://gitimmersion.com/index.html">Git Immersion</a> all take different approaches to learning Git, and each has a lot to offer. <a href="#fnref:2" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:3" role="doc-endnote">
<p>The command <code>git add -p</code> allows you to interactively group a series of interrelated revisions into one commit, even across multiple files. Think of changing the name of a lead character across many chapters. <code>git rebase -i</code> allows you to pick through a series of commits and, essentially, rewrite history into more logically structured commits with clearer commit messages. These are pretty advanced topics, so I don&rsquo;t recommend jumping right in with these commands. Lorna Mitchell&rsquo;s Git Workbook does a great job explaining both concepts. <a href="https://www.rakeroutes.com/deliberate-git/">Deliberate Git</a> also goes into using <code>rebase -i</code> to rewrite commits and commit messages to create a human readable <em>history</em> of a project, as opposed to a <em>log</em>. <a href="#fnref:3" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:4" role="doc-endnote">
<p><a href="https://git-scm.com/docs/git-diff">https://git-scm.com/docs/git-diff</a> <a href="#fnref:4" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:5" role="doc-endnote">
<p>Have I plugged her book enough yet? <a href="#fnref:5" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:6" role="doc-endnote">
<p>Once again, I love Lorna Mitchell&rsquo;s <a href="https://leanpub.com/gitworkbook">Git Workbook</a> because she sets up errors and then walks you through fixing them. <a href="#fnref:6" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:7" role="doc-endnote">
<p>While you are working, remember that <code>git status</code> lets you know what branch you are on, what changes Git is or isn&rsquo;t tracking, and what you have added to the staging area. After you commit, <code>git log</code> lets you look back to make sure everything happened the way that you expected, and lets you take a look at your commit messages. <a href="#fnref:7" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
</ol>
</section>
</article>
 
      

      <footer class="book-footer">
        
  <div class="flex flex-wrap justify-between">





</div>

 
        
      </footer>

      
  
  <div class="book-comments">

</div>
  
 

      <label for="menu-control" class="hidden book-menu-overlay"></label>
    </div>

    
    <aside class="book-toc">
      
  <nav id="TableOfContents">
  <ul>
    <li><a href="#introduction">Introduction</a></li>
    <li><a href="#get-started">Get Started</a>
      <ul>
        <li><a href="#tutorials-and-tools">Tutorials and Tools</a></li>
        <li><a href="#my-writing-process">My Writing Process</a></li>
        <li><a href="#four-commands-to-know-commit-diff-branch-and-merge">Four Commands to Know: <code>commit</code>, <code>diff</code>, <code>branch</code>, and <code>merge</code></a></li>
        <li><a href="#write-with-git">Write with Git</a></li>
      </ul>
    </li>
    <li><a href="#about-me">About Me</a></li>
    <li><a href="#the-poem">The Poem</a></li>
  </ul>
</nav>

 
    </aside>
    
  </main>

  
</body>

</html>












